a1 = int(input())
b1 = int(input())
c1 = int(input())
a2 = int(input())
b2 = int(input())
c2 = int(input())

if min(a1, b1, c1) == min(a2, b2, c2) and max(a1, b1, c1) == max(a2, b2, c2) and ((a1+b1+c1 - (min(a1, b1, c1)+max(a1, b1, c1))) == (a2 + b2 + c2 - (min(a2, b2, c2)+max(a2, b2, c2))):
    print('Boxes are equal')
else:
    if min(a1, b1, c1) < min(a2, b2, c2) and max(a1, b1, c1) < max(a2, b2, c2) and ((a1+b1+c1 - (min(a1, b1, c1)+max(a1, b1, c1))) < (a2 + b2 + c2-(min(a2, b2, c2)+max(a2, b2, c2))):
        print('The first box is smaller than the second one')
    if min(a1, b1, c1) > min(a2, b2, c2) and max(a1, b1, c1) > max(a2, b2, c2) and ((a1+b1+c1 - (min(a1, b1, c1)+max(a1, b1, c1))) > (a2 + b2 + c2-(min(a2, b2, c2)+max(a2, b2, c2))):
        print('The first box is larger than the second one')
    else:
        print('Boxes are incomparable')
