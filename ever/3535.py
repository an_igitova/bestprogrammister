n = int(input())

print('   _~_    ' * n)
print('  (o o)   ' * n)
print(' /  V  \  ' * n)
print('/(  _  )\ ' * n)
print('  ^^ ^^   ' * n)



'''
   _~_
  (o o)
 /  V  \
/(  _  )\
  ^^ ^^   
'''