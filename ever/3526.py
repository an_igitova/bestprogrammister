from math import ceil
k = int(input())
m = int(input())
n = int(input())

A = (n // k)
B = k - (n % k)
C = ceil((n - B) / k)

if k > n:
    print(m*2)
    exit(0)

if n%k == 0:
    print(m*A*2)
    exit(0)

print(m*(A) + m + m*(C))