A = str(input())
D = {}
for i in A:
    if i in D:
        D[i] += 1
    else:
        D[i] = 1
for i in D:
    if D[i] == 4:
        print('Yes')
    else:
        print('No')